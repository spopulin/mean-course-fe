import { Post } from './post.model';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class PostsService {
  private posts: Post[] = [];
  private postsUpdated = new Subject<Post[]>();

  postsUrl = 'http://localhost:3333/api/posts';

  constructor(private http: HttpClient) { }

  getPosts() {
    this.http.get<{ message: string, posts: Post[] }>(this.postsUrl).subscribe(
      (postsData) => {
        this.posts = postsData.posts;
        this.postsUpdated.next([...this.posts]);
      }
    );
  }

  getPostUpdateListener() {
    return this.postsUpdated.asObservable();
  }

  addPost(title: string, content: string) {
    const post: Post = { id: null, title: title, content: content };

    this.http.post<{ message: string }>(this.postsUrl, post).subscribe(
      (response) => {
        console.log(response)
        this.posts.push(post);
        this.postsUpdated.next([...this.posts]);
      }
    );

  }
}
